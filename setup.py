from setuptools import find_packages, setup

with open('version', 'r') as version_file:
    version = version_file.read().strip()

setup(
    name='new_project_for_alfiya',
    packages=find_packages(where='prod', exclude=['tests']),
    package_dir={'': 'prod'},
    version=version,
    description='new_project_for_alfiya',
    author='po_1 po_1'
)
